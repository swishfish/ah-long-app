package testingground;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.sunshineclub.ahlongapp.R;
import com.sunshineclub.ahlongapp.ioustuff.IOUDetailFragment;

public class TestingClass1 extends FragmentActivity {
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragactivity_testingclass1);

		// Check that the activity is using the layout version with
		// the fragment_container FrameLayout
		if (findViewById(R.id.fragment_container1) != null) {

			// However, if we're being restored from a previous state,
			// then we don't need to do anything and should return or else
			// we could end up with overlapping fragments.
			if (savedInstanceState != null) {
				return;
			}

			// Create an instance of ExampleFragment
			IOUDetailFragment firstFragment = new IOUDetailFragment();

			// In case this activity was started with special instructions from
			// an Intent,
			// pass the Intent's extras to the fragment as arguments
			//firstFragment.setArguments(getIntent().getExtras());
			
			//proof of concept working
			Bundle mBun = new Bundle();
			mBun.putBoolean("editable", false);
			firstFragment.setArguments(mBun);

			// Add the fragment to the 'fragment_container' FrameLayout
			getFragmentManager().beginTransaction().add(R.id.fragment_container1, firstFragment).commit();
			//getSupportFragmentManager().beginTransaction().add(R.id.fragment_container1, firstFragment).commit();
		}
	}
}
