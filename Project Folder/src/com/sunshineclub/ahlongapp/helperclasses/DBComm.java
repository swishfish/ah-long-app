package com.sunshineclub.ahlongapp.helperclasses;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

public class DBComm {

	// JSON parser class
	static JSONParser jsonParser = new JSONParser();

	public static final String ADDUSER_URL = "http://54.251.183.224/ahlongapp/adduser.php";
	private final static String ADDIOU_URL = "http://54.251.183.224/ahlongapp/addiou.php";
	private final static String OUTSTANDING_URL = "http://54.251.183.224/ahlongapp/outstandings.php";
	private final static String REMOVEIOU_URL = "http://54.251.183.224/ahlongapp/removeiou.php";
	private final static String FETCHIOU_URL = "http://54.251.183.224/ahlongapp/fetchiou.php";

	// ids
	public static final String TAG_SUCCESS = "success";
	public static final String TAG_MESSAGE = "message";
	public static final String TAG_OUTSTANDINGS = "outstandings";
	public static final String TAG_IOU_ID = "iouID";
	public static final String TAG_USERNAME = "username";
	public static final String TAG_USER_ID = "userID";
	public static final String TAG_PLACE = "nameOfPlace";
	public static final String TAG_LOCATION = "location";
	public static final String TAG_DATE = "iouDate";
	public static final String TAG_TOTAL_COST = "totalCost";
	public static final String TAG_PARTI = "iouParti";
	public static final String TAG_DEVICE_ID = "device_id";

	static ProgressDialog pDialog;

	public static String addIOUToServer(Context c, String place,
			String location, String date, ArrayList<String> partiArrList,
			String totalCost, String uid) throws NetworkOfflineException {

		String jsonarray = new String();

		jsonarray = jsonarray + "{";
		for (int x = 0; x < partiArrList.size(); x++) {
			jsonarray = jsonarray + "\"" + x + "\":" + "\""
					+ partiArrList.get(x) + "\"";

			if (x != partiArrList.size() - 1) {
				jsonarray = jsonarray + ",";
			}
		}
		jsonarray = jsonarray + "}";

		Log.d("json string output", jsonarray);

		// Building Parameters
		List<NameValuePair> params = new ArrayList<NameValuePair>();
		params.add(new BasicNameValuePair(TAG_PLACE, place));
		params.add(new BasicNameValuePair("location", location));
		params.add(new BasicNameValuePair("date", date));
		params.add(new BasicNameValuePair("partiArr", jsonarray));
		params.add(new BasicNameValuePair("totalCost", totalCost));
		params.add(new BasicNameValuePair("deviceid", uid.toString()));
		return sendDataToServer(c, params, ADDIOU_URL);

	}

	public static ArrayList<HashMap<String, String>> getOutstandingIOU(
			Context c, String device_id) throws NetworkOfflineException {

		if (!isNetworkOnline(c)) {
			// TODO make a toast or something
			throw new NetworkOfflineException("No internet la");
		} else {

			ArrayList<HashMap<String, String>> arrlist = new ArrayList<HashMap<String, String>>();

			// Bro, it's time to power up the J parser
			JSONParser jParser = new JSONParser();

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			// TODO make a static tag for device_id
			params.add(new BasicNameValuePair("device_id", device_id));

			// JSONObject json = jParser.getJSONFromUrl(OUTSTANDING_URL);
			JSONObject json = jsonParser.makeHttpRequest(OUTSTANDING_URL,
					"POST", params);

			// when parsing JSON stuff, we should probably
			// try to catch any exceptions:
			try {

				// I know I said we would check if "Posts were Avail."
				// (success==1)
				// before we tried to read the individual posts, but I lied...
				// mComments will tell us how many "posts" or comments are
				// available

				JSONArray mComments = json.getJSONArray(TAG_OUTSTANDINGS);

				// looping through all posts according to the json object
				// returned
				for (int i = 0; i < mComments.length(); i++) {

					JSONObject jsonObj = mComments.getJSONObject(i);

					Log.d("a jsonobject", c.toString());

					String[] attributes = { TAG_IOU_ID, TAG_USERNAME,
							TAG_USER_ID, TAG_PLACE, TAG_LOCATION,
							// TAG_DATE,
							TAG_TOTAL_COST };

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					for (String s : attributes) {

						// gets the content of each tag and put in map with
						// tagging
						// together
						map.put(s, jsonObj.getString(s));
					}

					arrlist.add(map);

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return arrlist;
		}

	}

	public static ArrayList<HashMap<String, String>> fetchIOUFromServer(
			Context c, String iouID, String did) throws NetworkOfflineException {

		if (!isNetworkOnline(c)) {
			// TODO make a toast or something
			throw new NetworkOfflineException("No internet la");
		} else {

			Log.d("something i wan to know0.5", "hi");

			ArrayList<HashMap<String, String>> arrList = new ArrayList<HashMap<String, String>>();
			JSONParser jParser = new JSONParser();

			List<NameValuePair> params = new ArrayList<NameValuePair>();

			params.add(new BasicNameValuePair(TAG_IOU_ID, iouID));
			params.add(new BasicNameValuePair(TAG_DEVICE_ID, did));

			JSONObject json = jsonParser.makeHttpRequest(FETCHIOU_URL, "POST",
					params);

			// when parsing JSON stuff, we should probably
			// try to catch any exceptions:
			try {

				// I know I said we would check if "Posts were Avail."
				// (success==1)
				// before we tried to read the individual posts, but I lied...
				// mComments will tell us how many "posts" or comments are
				// available

				JSONArray mComments = json.getJSONArray(TAG_PARTI);
				Log.d("something i wan to know1", mComments.toString());

				// looping through all posts according to the json object
				// returned
				for (int i = 0; i < mComments.length(); i++) {

					JSONObject jsonObj = mComments.getJSONObject(i);

					Log.d("something i wan to know2", c.toString());

					String[] attributes = { TAG_USERNAME, TAG_USER_ID };

					// creating new HashMap
					HashMap<String, String> map = new HashMap<String, String>();

					for (String s : attributes) {
						map.put(s, jsonObj.getString(s));
					}

					arrList.add(map);

				}

			} catch (JSONException e) {
				e.printStackTrace();
			}
			return arrList;
		}

	}

	private static String sendDataToServer(Context c,
			List<NameValuePair> params, String targetURL)
			throws NetworkOfflineException {

		if (!isNetworkOnline(c)) {
			// TODO make a toast or something
			throw new NetworkOfflineException("No internet la");
		} else {

			int success;
			try {

				Log.d("request!", "starting");

				// Posting user data to script
				JSONObject json = jsonParser.makeHttpRequest(targetURL, "POST",
						params);

				// full json response
				Log.d("Login attempt", json.toString());

				// json success element
				success = json.getInt(DBComm.TAG_SUCCESS);
				if (success == 1) {
					Log.d("User Created!", json.toString());
					// finish();
					return json.getString(TAG_MESSAGE);
				} else {
					Log.d("Login Failure!", json.getString(TAG_MESSAGE));
					return json.getString(TAG_MESSAGE);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return null;

		}

	}

	static public boolean checkUserExistInServer(String username) {
		// return true if user exist, return false if user is new
		return false;
	}

	public static boolean deleteIOU(Context c, String iouID)
			throws NetworkOfflineException {
		boolean success = false;

		if (!isNetworkOnline(c)) {
			// TODO make a toast or something
			throw new NetworkOfflineException("No internet la");
		} else {

			JSONParser jParser = new JSONParser();

			List<NameValuePair> params = new ArrayList<NameValuePair>();
			params.add(new BasicNameValuePair(TAG_IOU_ID, iouID));

			jParser.makeHttpRequest(REMOVEIOU_URL, "POST", params);

			success = true;

			return success;
		}
	}

	public static boolean isNetworkOnline(Context c) {
		boolean status = false;
		try {
			ConnectivityManager cm = (ConnectivityManager) c
					.getSystemService(Context.CONNECTIVITY_SERVICE);
			NetworkInfo netInfo = cm.getNetworkInfo(0);
			if (netInfo != null
					&& netInfo.getState() == NetworkInfo.State.CONNECTED) {
				status = true;
			} else {
				netInfo = cm.getNetworkInfo(1);
				if (netInfo != null
						&& netInfo.getState() == NetworkInfo.State.CONNECTED)
					status = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return status;

	}

}
