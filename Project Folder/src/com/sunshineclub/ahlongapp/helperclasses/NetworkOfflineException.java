package com.sunshineclub.ahlongapp.helperclasses;

public class NetworkOfflineException extends Exception {

	public NetworkOfflineException(String detailMessage) {
		super(detailMessage);
	}
}
