package com.sunshineclub.ahlongapp.ioustuff;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;

import com.sunshineclub.ahlongapp.R;
import com.sunshineclub.ahlongapp.helperclasses.DBComm;

public class IOUDetailFragment extends Fragment {

	private Calendar now;

	private EditText etPlace;
	private EditText etLocation;
	// TextView tvDateDisplay2;
	private EditText etTotalCost;

	private ListView lvParti;
	//private ArrayList<String> partiArrList;
	private ArrayAdapter<String> adapter1;

	private ArrayList<HashMap<String, String>> iouParti = new ArrayList<HashMap<String, String>>();

	private boolean editable = true;
	
	private void initialiseElements() {
		
		editable = this.getArguments().getBoolean("editable");

		etPlace = (EditText) getView().findViewById(R.id.etPlace2);
		etPlace.setEnabled(editable);

		etLocation = (EditText) getView().findViewById(R.id.etLocation2);
		etLocation.setEnabled(editable);

		etTotalCost = (EditText) getView().findViewById(R.id.etTotalCost2);
		etTotalCost.setEnabled(editable);

		lvParti = (ListView) getView().findViewById(R.id.lvParticipants2);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_iou_details, container,
				false);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);

		initialiseElements();

		etPlace.setText(getArguments().getString(DBComm.TAG_PLACE));

		etLocation.setText(getArguments().getString(DBComm.TAG_LOCATION));
		etTotalCost.setText(getArguments().getString(DBComm.TAG_TOTAL_COST));

		adapter1 = new ArrayAdapter<String>(
				getActivity(),
				android.R.layout.simple_list_item_1, 
				getArguments().getStringArrayList(DBComm.TAG_PARTI));
		lvParti.setAdapter(adapter1);
	}

}
