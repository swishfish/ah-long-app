package com.sunshineclub.ahlongapp.ioustuff;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Fragment;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.sunshineclub.ahlongapp.R;
import com.sunshineclub.ahlongapp.helperclasses.DBComm;
import com.sunshineclub.ahlongapp.helperclasses.NetworkOfflineException;

public class OpenIOU extends FragmentActivity {

	private ArrayList<HashMap<String, String>> iouParti = new ArrayList<HashMap<String, String>>();

	public Fragment anIouDetailFragment;

	private Bundle mBun;

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		// TODO create a new container xml infuture
		setContentView(R.layout.fragactivity_testingclass1);

		// Check that the activity is using the layout version with
		// the fragment_container FrameLayout
		if (findViewById(R.id.fragment_container1) != null) {

			// However, if we're being restored from a previous state,
			// then we don't need to do anything and should return or else
			// we could end up with overlapping fragments.
			if (savedInstanceState != null) {
				return;
			}

			// Create an instance of ExampleFragment
			anIouDetailFragment = new IOUDetailFragment();

			// In case this activity was started with special instructions from
			// an Intent,
			// pass the Intent's extras to the fragment as arguments
			// firstFragment.setArguments(getIntent().getExtras());
			mBun = new Bundle();
			mBun.putBoolean("editable", false);
			mBun.putString(DBComm.TAG_PLACE,
					getIntent().getExtras().getString(DBComm.TAG_PLACE));
			mBun.putString(DBComm.TAG_LOCATION, getIntent().getExtras()
					.getString(DBComm.TAG_LOCATION));
			mBun.putString(DBComm.TAG_TOTAL_COST, getIntent().getExtras()
					.getString(DBComm.TAG_TOTAL_COST));
			

		}

		new FetchIOU().execute();
	}

	private class FetchIOU extends AsyncTask<Void, Void, Boolean> {

		// TODO might wan to consider putting this outside this class?
		ProgressDialog pDialog = new ProgressDialog(OpenIOU.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog.setMessage("Fetching IOU...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected Boolean doInBackground(Void... arg0) {

			try {
				iouParti = DBComm.fetchIOUFromServer(OpenIOU.this, getIntent().getExtras()
						.getString(DBComm.TAG_IOU_ID), getIntent().getExtras()
						.getString("did"));
			} catch (NetworkOfflineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			ArrayList<String> partiArrList = new ArrayList<String>();

			for (int x = 0; x < iouParti.size(); x++) {
				partiArrList.add(iouParti.get(x).get(DBComm.TAG_USERNAME));
			}

			//adapter1 = new ArrayAdapter<String>(ViewIOU.this,android.R.layout.simple_list_item_1, partiArrList);
			//lvParti.setAdapter(adapter1);

			
			mBun.putStringArrayList(DBComm.TAG_PARTI, partiArrList);
			
			anIouDetailFragment.setArguments(mBun);
			
			// Add the fragment to the 'fragment_container' FrameLayout
			getFragmentManager().beginTransaction()
					.add(R.id.fragment_container1, anIouDetailFragment)
					.commit();
			// getSupportFragmentManager().beginTransaction().add(R.id.fragment_container1,
			// firstFragment).commit();

		}

	}

	
	/*
	public void updateDateDisplay(int year, int month, int day) {
		tvDateDisplay2.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(month + 1).append("/").append(day).append("/")
				.append(year).append(" "));
	}
	*/
}
