package com.sunshineclub.ahlongapp.ioustuff;

import java.util.ArrayList;
import java.util.Calendar;

import android.app.DialogFragment;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.sunshineclub.ahlongapp.DatePickerDialogFragment;
import com.sunshineclub.ahlongapp.R;
import com.sunshineclub.ahlongapp.helperclasses.DBComm;
import com.sunshineclub.ahlongapp.helperclasses.DeviceUuidFactory;
import com.sunshineclub.ahlongapp.helperclasses.NetworkOfflineException;

public class NewIOU extends FragmentActivity {

	private Calendar now;

	private EditText etPlace, etLocation, etTotalCost;
	private TextView tvDateDisplay;

	private ArrayList<String> partiArrList;
	private ArrayAdapter<String> adapter1;

	private EditText etAddParti;
	private Button bAddParti;
	private ListView lvParti;

	private Button bSubmit;

	private DeviceUuidFactory uid;

	// private BigDecimal totalCost;

	private void initialiseElements() {

		etPlace = (EditText) findViewById(R.id.etPlace);
		etLocation = (EditText) findViewById(R.id.etLocation);
		tvDateDisplay = (TextView) findViewById(R.id.tvDateDisplay);

		etTotalCost = (EditText) findViewById(R.id.etTotalCost);

		etAddParti = (EditText) findViewById(R.id.etAddParticipant);
		bAddParti = (Button) findViewById(R.id.bAddParticipant);
		lvParti = (ListView) findViewById(R.id.lvParticipants);

		bSubmit = (Button) findViewById(R.id.bSubmit);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_new_iou);

		initialiseElements();

		setCurrentDateOnView();

		// launches DatePickerDialogFragment upon clicking on the tvDateDisplay
		tvDateDisplay.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				showDatePickerDialogFragment();
			}
		});

		partiArrList = new ArrayList<String>();

		adapter1 = new ArrayAdapter<String>(this,
				android.R.layout.simple_list_item_1, partiArrList);
		lvParti.setAdapter(adapter1);

		bAddParti.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				addParti();
			}
		});

		etAddParti.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				// TODO Auto-generated method stub

				if (keyCode == KeyEvent.KEYCODE_ENTER)
					addParti();

				if (keyCode == KeyEvent.KEYCODE_BACK)
					finish();

				return true;
			}
		});

		bSubmit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				/*
				 * 4) Attached media (future bg sync) *WIP* 5) IOU objects *WIP*
				 */

				uid = new DeviceUuidFactory(NewIOU.this);
				// totalCost = new BigDecimal(etTotalCost.getText().toString());
				new CreateIOU().execute();

				// b) return to MainMenu Activity

				// wad i am doing now is launch new Activity.
				// nt sure if we can return to a previous activity anot
				Intent aIntent;
				try {
					aIntent = new Intent(NewIOU.this, Class
							.forName("com.sunshineclub.ahlongapp.MainMenu"));
					startActivity(aIntent);
					finish();
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
	}

	private class CreateIOU extends AsyncTask<String, String, String> {

		// TODO might wan to consider putting this outside this class?
		ProgressDialog pDialog = new ProgressDialog(NewIOU.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog.setMessage("Creating IOU...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected String doInBackground(String... params) {

			String result = null;
			try {
				result = DBComm.addIOUToServer(NewIOU.this, etPlace.getText().toString(),
						etLocation.getText().toString(), tvDateDisplay.getText()
								.toString(), partiArrList, etTotalCost.getText()
								.toString(), uid.getDeviceUuid().toString());
			} catch (NetworkOfflineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return result;
		}

		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted

			pDialog.dismiss();

			if (file_url != null) {
				Toast.makeText(NewIOU.this, file_url, Toast.LENGTH_LONG).show();
			}

			finish();
		}

	}

	public void showDatePickerDialogFragment() {
		DialogFragment newFragment = DatePickerDialogFragment.newInstance(this,
				new DatePickerDialogFragmentListener() {

					@Override
					public void updateChangedDate(int year, int month, int day) {
						// TODO Auto-generated method stub
						updateDateDisplay(year, month, day);
					}

				}, now);

		newFragment.show(getFragmentManager(), "DatePickerDialogFragment");
	}

	public void updateDateDisplay(int year, int month, int day) {
		tvDateDisplay.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(month + 1).append("/").append(day).append("/")
				.append(year).append(" "));
	}

	private void setCurrentDateOnView() {

		int year;
		int month;
		int day;

		now = Calendar.getInstance();
		year = now.get(Calendar.YEAR);
		month = now.get(Calendar.MONTH);
		day = now.get(Calendar.DAY_OF_MONTH);

		// set current date into textview
		updateDateDisplay(year, month, day);
	}

	protected void addParti() {

		if (isInputValid(etAddParti)) {
			partiArrList.add(0, etAddParti.getText().toString());
			etAddParti.setText("");
			adapter1.notifyDataSetChanged();
		}
	}

	protected boolean isInputValid(EditText etInput2) {
		etInput2.setError(null);
		if (etInput2.getText() != null) {
			if (etInput2.getText().toString().trim().length() < 1) {
				etInput2.setError("Please Enter Item");
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public interface DatePickerDialogFragmentListener {
		// this interface is a listener between the Date Dialog fragment and the
		// activity to update the buttons date
		public void updateChangedDate(int year, int month, int day);
	}
}
