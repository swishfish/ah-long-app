package com.sunshineclub.ahlongapp.ioustuff;

import java.math.BigDecimal;
import java.util.ArrayList;

import com.sunshineclub.ahlongapp.Item;
import com.sunshineclub.ahlongapp.User;

public class IOU {
	private String iouID;
	
	private BigDecimal totalCost, remainingAmt;
	
	ArrayList<User> userList;
	ArrayList<Item> itemList;
	//User ahLong;
	
	private String ahLongUsername;
	private String ahLongUserID;
	private String nameOfPlace;
	private String location;

	public IOU(String iouID, String ahLongUsername, String ahLongUserID, String nameOfPlace, String location, BigDecimal totalCost) {
		this.iouID = iouID;
		this.ahLongUsername = ahLongUsername;
		this.ahLongUserID = ahLongUserID;
		this.nameOfPlace = nameOfPlace;
		this.location = location;
		this.totalCost = totalCost;
		
		//TODO initialize the user and item lists
	}

	public void addItemToList(Item i) {
		//TODO implement method
	}
	
	public void claimItemFromList(int listPosition) {
		//TODO implement method
	}
	
	public void removeItemFromList(int listPosition) {
		//TODO implement method
	}

	public String getIouID() {
		return iouID;
	}

	public void setIouID(String iouID) {
		this.iouID = iouID;
	}

	public BigDecimal getTotalCost() {
		return totalCost;
	}

	public void setTotalCost(BigDecimal totalCost) {
		this.totalCost = totalCost;
	}

	public BigDecimal getRemainingAmt() {
		return remainingAmt;
	}

	public void setRemainingAmt(BigDecimal remainingAmt) {
		this.remainingAmt = remainingAmt;
	}

	public ArrayList<User> getUserList() {
		return userList;
	}

	public void setUserList(ArrayList<User> userList) {
		this.userList = userList;
	}

	public ArrayList<Item> getItemList() {
		return itemList;
	}

	public void setItemList(ArrayList<Item> itemList) {
		this.itemList = itemList;
	}

	public String getAhLongUsername() {
		return ahLongUsername;
	}

	public void setAhLongUsername(String ahLongUsername) {
		this.ahLongUsername = ahLongUsername;
	}

	public String getAhLongUserID() {
		return ahLongUserID;
	}

	public void setAhLongUserID(String ahLongUserID) {
		this.ahLongUserID = ahLongUserID;
	}

	public String getNameOfPlace() {
		return nameOfPlace;
	}

	public void setNameOfPlace(String nameOfPlace) {
		this.nameOfPlace = nameOfPlace;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}
	
	
	
}