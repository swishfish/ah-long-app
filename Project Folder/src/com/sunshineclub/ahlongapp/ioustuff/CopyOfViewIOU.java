package com.sunshineclub.ahlongapp.ioustuff;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.sunshineclub.ahlongapp.R;
import com.sunshineclub.ahlongapp.helperclasses.DBComm;
import com.sunshineclub.ahlongapp.helperclasses.DeviceUuidFactory;
import com.sunshineclub.ahlongapp.helperclasses.NetworkOfflineException;

public class CopyOfViewIOU extends Activity {

	private Calendar now;

	TextView tvPlace;
	TextView tvLocation;
	TextView tvDateDisplay2;
	TextView tvTotalCost;

	private ListView lvParti;
	private ArrayList<String> partiArrList;
	private ArrayAdapter<String> adapter1;

	private ArrayList<HashMap<String, String>> iouParti = new ArrayList<HashMap<String, String>>();

	private void initialiseElements() {

		tvPlace = (TextView) findViewById(R.id.tvPlace);
		tvLocation = (TextView) findViewById(R.id.tvLocation);
		tvTotalCost = (TextView) findViewById(R.id.tvTotalCost);
		lvParti = (ListView) findViewById(R.id.lvParti);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_open_iou);

		initialiseElements();

		tvPlace.setText(getIntent().getExtras().getString(DBComm.TAG_PLACE));
		tvLocation.setText(getIntent().getExtras().getString(
				DBComm.TAG_LOCATION));
		tvTotalCost.setText("Total cost: $"
				+ getIntent().getExtras().getString(DBComm.TAG_TOTAL_COST));

		new FetchIOU().execute();
	}

	public void updateDateDisplay(int year, int month, int day) {
		tvDateDisplay2.setText(new StringBuilder()
				// Month is 0 based so add 1
				.append(month + 1).append("/").append(day).append("/")
				.append(year).append(" "));
	}

	private class FetchIOU extends AsyncTask<Void, Void, Boolean> {

		// TODO might wan to consider putting this outside this class?
		ProgressDialog pDialog = new ProgressDialog(CopyOfViewIOU.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();

			pDialog.setMessage("Fetching IOU...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();

		}

		@Override
		protected Boolean doInBackground(Void... arg0) {

			try {
				iouParti = DBComm.fetchIOUFromServer(null, getIntent().getExtras()
						.getString(DBComm.TAG_IOU_ID), getIntent().getExtras()
						.getString("did"));
			} catch (NetworkOfflineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			return true;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();

			ArrayList<String> partiArrList = new ArrayList<String>();

			for (int x = 0; x < iouParti.size(); x++) {
				partiArrList.add(iouParti.get(x).get(DBComm.TAG_USERNAME));
			}

			adapter1 = new ArrayAdapter<String>(CopyOfViewIOU.this,
					android.R.layout.simple_list_item_1, partiArrList);
			lvParti.setAdapter(adapter1);

		}

	}

}
