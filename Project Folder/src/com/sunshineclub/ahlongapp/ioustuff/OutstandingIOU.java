package com.sunshineclub.ahlongapp.ioustuff;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListAdapter;
import android.widget.SimpleAdapter;

import com.sunshineclub.ahlongapp.R;
import com.sunshineclub.ahlongapp.helperclasses.DBComm;
import com.sunshineclub.ahlongapp.helperclasses.DeviceUuidFactory;
import com.sunshineclub.ahlongapp.helperclasses.NetworkOfflineException;

public class OutstandingIOU extends ListActivity {

	
	private DeviceUuidFactory did;
	
	ArrayList<HashMap<String, String>> outstandingIOU = null;
	ListAdapter adapter1 = null;

	private String removeId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		did = new DeviceUuidFactory(this);
		
		new GetOutstanding().execute();
	

		getListView().setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View v, int position,
					long id) {

				outstandingIOU.get(position);
				Intent startViewIOU = new Intent("com.sunshineclub.ahlongapp.VIEWIOU");
				Bundle mBun = new Bundle();
				mBun.putString(DBComm.TAG_PLACE, outstandingIOU.get(position).get(DBComm.TAG_PLACE));
				mBun.putString(DBComm.TAG_LOCATION, outstandingIOU.get(position).get(DBComm.TAG_LOCATION));
				mBun.putString(DBComm.TAG_IOU_ID, outstandingIOU.get(position).get(DBComm.TAG_IOU_ID));
				mBun.putString(DBComm.TAG_TOTAL_COST, outstandingIOU.get(position).get(DBComm.TAG_TOTAL_COST));
				mBun.putString("did", did.getDeviceUuid().toString());
				startViewIOU.putExtras(mBun);

				startActivity(startViewIOU);

			}

		});

		getListView().setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View v,
					int position, long id) {

				//TODO include are u sure dialog in future
				
				
				
				Log.d("xuatz text", outstandingIOU.get(position).get(DBComm.TAG_IOU_ID));
				removeId = outstandingIOU.get(position).get(DBComm.TAG_IOU_ID);
				outstandingIOU.remove(position);
				new RemoveIOU().execute();
				
				
				
				return true;
			}

		});

	}

	@Override
	protected void onResume() {
		super.onResume();

		// TODO

	}

	private void updateOutstandingList(ArrayList<String> localList) {
		// TODO obtain new list frm server

		// TODO DT
		// ArrayList remoteList = DTCLASS.getOutstandingList();
		ArrayList<String> remoteList = null;

		if (listChanged(localList, remoteList)) {
			localList = remoteList;
			// TODO nt sure if listadapter will be update automatically
		}
	}

	private boolean listChanged(ArrayList<String> localList,
			ArrayList<String> remoteList) {
		// TODO operations
		return false;
	}

	
	public class GetOutstanding extends AsyncTask<Void, Void, Boolean> {

		// TODO might wan to consider putting this outside this class?
		ProgressDialog pDialog = new ProgressDialog(OutstandingIOU.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(OutstandingIOU.this);
			pDialog.setMessage("Getting IOUs...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			try {
				outstandingIOU = DBComm.getOutstandingIOU(OutstandingIOU.this, did.getDeviceUuid().toString());
			} catch (NetworkOfflineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (outstandingIOU == null) {
				Log.d("Teh1", "gg");
			} else {
				Log.d("Teh2", outstandingIOU.get(0).get(DBComm.TAG_PLACE));
			}
			return true;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			adapter1 = new SimpleAdapter(OutstandingIOU.this, outstandingIOU,
					R.layout.single_post, new String[] { DBComm.TAG_PLACE,
							DBComm.TAG_LOCATION, DBComm.TAG_TOTAL_COST },
					new int[] { R.id.place, R.id.location, R.id.totalCost });

			setListAdapter(adapter1);
		}
		
		
	}
	

	//TODO relocate this non-ideal inner class placement in future
	public class RemoveIOU extends AsyncTask<Void, Void, Boolean> {

		// TODO might wan to consider putting this outside this class?
		ProgressDialog pDialog = new ProgressDialog(OutstandingIOU.this);

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(OutstandingIOU.this);
			pDialog.setMessage("Deleting IOU...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected Boolean doInBackground(Void... arg0) {
			Log.d("xz-iouid", removeId);	
			try {
				DBComm.deleteIOU(OutstandingIOU.this, removeId);
			} catch (NetworkOfflineException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return true;

		}

		@Override
		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			pDialog.dismiss();
			adapter1 = new SimpleAdapter(OutstandingIOU.this, outstandingIOU,
					R.layout.single_post, new String[] { DBComm.TAG_PLACE,
							DBComm.TAG_LOCATION, DBComm.TAG_TOTAL_COST },
					new int[] { R.id.place, R.id.location, R.id.totalCost });

			setListAdapter(adapter1);
		}
	}	
}
