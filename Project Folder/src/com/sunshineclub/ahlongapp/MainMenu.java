package com.sunshineclub.ahlongapp;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainMenu extends ListActivity {

	String[] operationsArr = null;
	Class aClass = null;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		operationsArr = getResources().getStringArray(R.array.operations_array);
		setListAdapter(new ArrayAdapter<String>(MainMenu.this,
				android.R.layout.simple_list_item_1, operationsArr));

	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);

		String className = operationsArr[position];

		if (className.contains("IOU")) {
			try {
				aClass = Class.forName("com.sunshineclub.ahlongapp.ioustuff."
						+ className);
				Intent aIntent = new Intent(MainMenu.this, aClass);
				startActivity(aIntent);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (className.contains("TestingClass1")) {
			try {
				aClass = Class.forName("testingground."
						+ className);
				Intent aIntent = new Intent(MainMenu.this, aClass);
				startActivity(aIntent);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else
			try {
				aClass = Class.forName("com.sunshineclub.ahlongapp."
						+ className);
				Intent aIntent = new Intent(MainMenu.this, aClass);
				startActivity(aIntent);
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
