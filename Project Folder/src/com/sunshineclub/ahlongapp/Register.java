package com.sunshineclub.ahlongapp;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.sunshineclub.ahlongapp.helperclasses.DBComm;
import com.sunshineclub.ahlongapp.helperclasses.DeviceUuidFactory;
import com.sunshineclub.ahlongapp.helperclasses.JSONParser;

public class Register extends Activity implements OnClickListener {

	private EditText etUsername, etPassword, etPassword2, etEmail;
	private Button bSubmit, bBypass;

	// Progress Dialog
	private ProgressDialog pDialog;

	private DeviceUuidFactory uid;
	
	// JSON parser class
	private JSONParser jsonParser = new JSONParser();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		/*checkLoginStatus();*/
		setContentView(R.layout.activity_register);

		etUsername = (EditText) findViewById(R.id.etUsername);
		etPassword = (EditText) findViewById(R.id.etPassword);
		etPassword2 = (EditText) findViewById(R.id.etPassword2);
		etEmail = (EditText) findViewById(R.id.etEmail);
		
		bSubmit = (Button) findViewById(R.id.bSubmit);
		bBypass = (Button) findViewById(R.id.bBypass);

		bSubmit.setOnClickListener(this);
		
		bBypass.setOnClickListener(new OnClickListener() 
			{	@Override
				public void onClick(View v) {
				callMainMenu();
				finish();
				}
			}
		);
		

	}

	@Override
	public void onClick(View v) {
		if (DBComm.checkUserExistInServer(etUsername.getText().toString())) {
			etUsername.setText(null);
			etUsername.requestFocus();
			Toast.makeText(getApplicationContext(),
					"Username is taken, please choose another.",
					Toast.LENGTH_LONG).show();
		}

		else if (!checkTwoPasswordInputs()) {
			etPassword2.setText(null);
			etPassword2.requestFocus();
			Toast.makeText(
					getApplicationContext(),
					"Second password is different from the first. Please re-enter the same password.",
					Toast.LENGTH_LONG).show();
		}

		else if (!checkEmailAuthenticity(etEmail.getText().toString())) {
			etEmail.setText(null);
			etEmail.requestFocus();
			Toast.makeText(getApplicationContext(),
					"Email is invalid, please enter a valid email.",
					Toast.LENGTH_LONG).show();
		}

		else {
			uid = new DeviceUuidFactory(this);
			new CreateUser().execute();
			callMainMenu();
			
			/*SaveSharedPreference.setUserName(getApplicationContext(), etUsername.getText().toString());*/
		}
	}

	protected static boolean checkEmailAuthenticity(CharSequence target) {
		if (target.equals(null)) {
			// if there's no input from user, return false
			return false;
		} else {
			// uses built in email checker to check email.
			// it is still necessary to send confirmation email to address

			return android.util.Patterns.EMAIL_ADDRESS.matcher(target)
					.matches();
		}
	}

	protected boolean checkTwoPasswordInputs() {
		if (etPassword.getText().toString()
				.equals(etPassword2.getText().toString())) {
			return true;
		} else {
			return false;
		}
	}

	class CreateUser extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		boolean failure = false;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			pDialog = new ProgressDialog(Register.this);
			pDialog.setMessage("Creating User...");
			pDialog.setIndeterminate(false);
			pDialog.setCancelable(true);
			pDialog.show();
		}

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			// Check for success tag
			int success;
			String username = etUsername.getText().toString();
			String password = etPassword.getText().toString();
			String email = etEmail.getText().toString();

			try {
				// Building Parameters
				List<NameValuePair> params = new ArrayList<NameValuePair>();
				params.add(new BasicNameValuePair("username", username));
				params.add(new BasicNameValuePair("password", password));
				params.add(new BasicNameValuePair("email", email));
				params.add(new BasicNameValuePair("device_id", uid
						.getDeviceUuid().toString()));

				Log.d("request!", "starting");

				// Posting user data to script
				JSONObject json = jsonParser.makeHttpRequest(
						DBComm.ADDUSER_URL, "POST", params);

				// full json response
				Log.d("Login attempt", json.toString());

				// json success element
				success = json.getInt(DBComm.TAG_SUCCESS);
				if (success == 1) {
					Log.d("User Created!", json.toString());
					finish();
					return json.getString(DBComm.TAG_MESSAGE);
				} else {
					Log.d("Login Failure!", json.getString(DBComm.TAG_MESSAGE));
					return json.getString(DBComm.TAG_MESSAGE);

				}
			} catch (JSONException e) {
				e.printStackTrace();
			}

			return null;

		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String file_url) {
			// dismiss the dialog once product deleted
			pDialog.dismiss();
			if (file_url != null) {
				Toast.makeText(Register.this, file_url, Toast.LENGTH_LONG)
						.show();
			}

		}

	}
	
	protected void callMainMenu() {
		try {
			Intent aIntent = new Intent(Register.this, Class
					.forName("com.sunshineclub.ahlongapp.MainMenu"));
			startActivity(aIntent);
			/*finish();*/
			}
			catch (Exception e) {
				Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
			}
		
	}
/*	protected void checkLoginStatus() {
		if(SaveSharedPreference.getUserName(Register.this).length() == 0)
		{
		     //do nothing
		}
		else
		{
			try {
				Intent aIntent = new Intent(Register.this, Class
						.forName("com.sunshineclub.ahlongapp.MainMenu"));
				startActivity(aIntent);
				finish();
				}
				catch (Exception e) {
					Toast.makeText(getApplicationContext(), e.toString(), Toast.LENGTH_LONG).show();
				}
		}
	}*/

}