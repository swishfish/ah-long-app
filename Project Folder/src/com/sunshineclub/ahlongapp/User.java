package com.sunshineclub.ahlongapp;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

import com.sunshineclub.ahlongapp.ioustuff.IOU;


public class User implements Serializable {
	private String userID;
	String name;
	String email;
	private String password;
	Timestamp dateOfCreation;
	
	ArrayList <IOU> createdIOUs = new ArrayList <IOU> ();
	ArrayList <IOU> taggedIOUs = new ArrayList <IOU> ();
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public User(String userID, String name, String email, String password) {
		this.dateOfCreation = getTimeStamp();
		this.name = name;
		this.email = email;
		this.password = password;
		
	}

	private Timestamp getTimeStamp() {
		Date date = new Date();
		return new Timestamp(date.getTime());
		
	}
}
