package com.sunshineclub.ahlongapp;

import java.math.BigDecimal;

public class Item {
	String name;
	BigDecimal cost;
	
	public Item(String name, BigDecimal cost) {
		this.name = name;
		this.cost = cost;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BigDecimal getCost() {
		return cost;
	}

	public void setCost(BigDecimal cost) {
		this.cost = cost;
	}
	
	
}
